/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author ericzheng
 *
 */
public class Fahrenheit extends Temperature 
{ 
	 public Fahrenheit(float t) 
	 { 
	 super(t); 
	 } 
	 public String toString() 
	 { 
	 // TODO: Complete this method 
	 return "The Fahrenheit is "+ this.getValue(); 
	 }
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float c;
		c = (this.getValue())-32;
		c = c* 5;
		c = c / 9;
		return new Celsius(c);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	} 
}